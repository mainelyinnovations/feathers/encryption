import { strict as assert } from "assert"
import { feathers } from "@feathersjs/feathers"
import { encryption } from "../src";

describe("@mainelyio/feathers-encryption", () => {
  describe("setup", () => {
    it("errors when service does not exist", async () => {
      const app = feathers()
      
      const oldSetup = app.setup
      app.setup = function (arg: any) {
        return oldSetup.call(this, arg)
      }

      assert.rejects(
        async () => {
          app.hooks({
            setup: [
              encryption()
            ]
          })

          await app.setup()
        },
        {
          message: "No encryption config found"
        }
      )
    })

    it("errors when service is not configured correctly", async () => {
      const app = feathers()
      
      const oldSetup = app.setup
      app.setup = function (arg: any) {
        return oldSetup.call(this, arg)
      }

      app.set("encryptionConfig", { secret: "secret", method: "aes-256-cbc" })

      assert.rejects(
        async () => {
          app.hooks({
            setup: [
              encryption()
            ]
          })

          await app.setup()
        },
        {
          message: "Encryption key, secret, and method are require2d"
        }
      )
    })

    it("does not error when service is configured", async () => {
      const app = feathers()
      
      const oldSetup = app.setup
      app.setup = function (arg: any) {
        return oldSetup.call(this, arg)
      }

      app.set("encryptionConfig", { key: "key", secret: "secret", method: "aes-256-cbc" })
      app.hooks({
        setup: [
          encryption()
        ]
      })

      await app.setup()
    })
  })
})
