import { strict as assert } from "assert"
import { feathers } from "@feathersjs/feathers"
import { encryption } from "../src";

describe("@mainelyio/feathers-encryption service", () => {
  const expectedData = "sensitive-value";
  const expectedEncrypted = "ZGIxOWMzN2Y3NTg4NTYzMDMwYWQ0MmJhYjliMDU2YWQ="

  it("encrypts data successfully", async () => {
    const app = feathers()
      
    const oldSetup = app.setup
    app.setup = function (arg: any) {
      return oldSetup.call(this, arg)
    }
  
    app.set("encryptionConfig", { key: "key", secret: "secret", method: "aes-256-cbc" })
    app.hooks({
      setup: [
        encryption()
      ]
    })

    await app.setup()

    const crypto = app.get("encryption");
    assert.equal(expectedEncrypted, crypto.encryptData(expectedData))
  })

  it("decrypts data successfully", async () => {
    const app = feathers()
      
    const oldSetup = app.setup
    app.setup = function (arg: any) {
      return oldSetup.call(this, arg)
    }

    app.set("encryptionConfig", { key: "key", secret: "secret", method: "aes-256-cbc" })
    app.hooks({
      setup: [
        encryption()
      ]
    })

    await app.setup()

    const crypto = app.get("encryption");
    const encrypted = crypto.encryptData(expectedData);
    const decrypted = crypto.decryptData(encrypted);

    assert.equal(expectedData, decrypted)
  })
})
