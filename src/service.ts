import crypto from "crypto";

export type IEncryption = {
  encryptData: (data: any) => string;
  decryptData: (encryptedData: any) => string;
}

export type IEncryptionConfig = {
  key: string;
  secret: string;
  method: string;
}

export class Encryption implements IEncryption {
  key: string;
  encryptionIV: string;
  encryptionMethod: string;

  constructor(config: IEncryptionConfig) {
    // Generate secret hash with crypto to use for encryption
    this.key = crypto.createHash("sha512").update(config.key).digest("hex").substring(0, 32);
    this.encryptionIV = crypto.createHash("sha512").update(config.secret).digest("hex").substring(0, 16);
    this.encryptionMethod = config.method;
  }

  encryptData(data: any) {
    const cipher = crypto.createCipheriv(this.encryptionMethod, this.key, this.encryptionIV);

    // Encrypts data and converts to hex and base64
    return Buffer.from(cipher.update(data, "utf8", "hex") + cipher.final("hex")).toString("base64");
  }

  decryptData(encryptedData: any) {
    const buff = Buffer.from(encryptedData, "base64");
    const decipher = crypto.createDecipheriv(this.encryptionMethod, this.key, this.encryptionIV);

    // Decrypts data and converts to utf8
    return decipher.update(buff.toString("utf8"), "hex", "utf8") + decipher.final("utf8");
  }
}
