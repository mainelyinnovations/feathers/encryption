import { HookContext, NextFunction } from "@feathersjs/feathers";

import { Encryption, IEncryption, IEncryptionConfig } from "./service";
export { IEncryption, IEncryptionConfig };

export const encryption = ({ configName, className } = { className: "encryption", configName: "encryptionConfig" }) => {
  return async (context: HookContext, next: NextFunction) => {
    const config: IEncryptionConfig = context.app.get(configName);

    if (!config) {
      throw new Error("No encryption config found");
    }

    if (!config.key || !config.secret || !config.method) {
      throw new Error("Encryption key, secret, and method are required");
    }

    const encryption: IEncryption = new Encryption(config);
    context.app.set(className, encryption);
    await next();
  }
}
