# @mainelyio/feathers-encryption

[![Download Status](https://img.shields.io/npm/dm/@mainelyio/feathers-encryption.svg?style=flat-square)](https://www.npmjs.com/package/@mainelyio/feathers-encryption)

> FeathersJS encryption plugin for encrypting & decrypting data.

## Installation

```bash
npm install @mainelyio/feathers-encryption --save
```

## Documentation

To come

## Configuration

In order to use the module, the following configuration must be in place:

```js
{
  key: "my-key",
  secret: "my-secret",
  method: "aes-256-cbc"
}
```

To set the data, you may use the following command:

```js
app.set("encryptionConfig", {
  key: "my-key",
  secret: "my-secret",
  method: "aes-256-cbc"
});
```

## Usage

The purpose of the package is to be used with resolvers in setting & retrieving data from databases, to ensure sensitive data is not stored in plain text.

To encrypt data for rest, you may use a resolver like this:

```js
export const myServiceResolver = resolve<MyService, HookContext>({
  secretId: async (value, tenant, context) => {
    const encryption = context.app.get("encryption");
    return encryption.decryptData(value);
  }
});
```

To decrypt data when querying a database:

```js
export const azureTenantDataResolver = resolve<MyService, HookContext>({
  secretId: async (value, tenant, context) => {
    const encryption = context.app.get("encryption");
    return encryption.encryptData(value);
  }
});
```

## License

Licensed under the [MIT license](LICENSE).
